from click import command, option, group, secho, prompt, confirm

from kartography.core.aws_lake import AWSLake
from kartography.commands.aws_map import select_buckets, show_characteristics

BC = "black"
FC = "cyan"
ERROR_C = "red"
WARNING_C = "yellow"
SUCCESS_C = "green"
space = " "
ACTIONS = {"1": "characteristics", "4": "map"}
SHOW_TYPES = {"1": "json", "2": "print"}


def get_select_buckets(buckets_list: list) -> list:
    # select options:
    # --no-dev, --all, --all-except, --only
    secho("You can choose between the following options", bg=BC, fg=FC)
    secho("In order to use 'only' and 'all-except' you have to input the numbers", bg=BC, fg=FC)
    secho("of the buckets previously shown separated by commas", bg=BC, fg=FC)
    secho("for example: only 0,5", bg=BC, fg=FC)
    _input = prompt("no-dev, all, all-except, only")
    params = _input.split(" ")
    if len(params) == 1:
        selected_buckets, error = select_buckets(buckets=buckets_list, selected_list="", command=params[0])
    elif len(params) == 2:
        selected_buckets, error = select_buckets(buckets=buckets_list, selected_list=params[1], command=params[0])
    else:
        secho("Error the value is not correct please try to execute again list-buckets", bg=BC, fg=ERROR_C)
        return []

    if error != "":
        secho(error, bg=BC, fg=ERROR_C)
        return []
    secho(
            f"You have selected: {selected_buckets}",
            bg=BC, fg=SUCCESS_C
    )

    return selected_buckets


def choose_action(current_lake, buckets, action, show_type):
    if ACTIONS.get(action) == "characteristics":
        show_characteristics(current_lake, buckets, SHOW_TYPES.get(show_type))
    elif ACTIONS.get(action) == "map":
        print("MAP")


def show_action_menu(buckets, data_lake):
    """Options menu with buckets selected
    1. show buckets characteristics
    2. show buckets info
    3. show buckets percentages
    4. show buckets pretty map
    """
    secho("You can choose between the following options", bg=BC, fg=FC)
    secho("1. show buckets characteristics", bg=BC, fg=FC)
    secho("2. show buckets info", bg=BC, fg=FC)
    secho("3. show buckets percentages", bg=BC, fg=FC)
    secho("4. show buckets pretty map", bg=BC, fg=FC)
    _input = prompt("1,2,3,4")
    secho(f"{_input}", bg=BC, fg=SUCCESS_C)

    secho("How should I show you the result?", bg=BC, fg=FC)
    secho("1. save it on a json file", bg=BC, fg=FC)
    secho("2. print it here", bg=BC, fg=FC)
    secho("3. save it on a csv file", bg=BC, fg=FC)
    show_type = prompt("1,2,3")
    secho(f"{show_type}", bg=BC, fg=SUCCESS_C)
    choose_action(data_lake, buckets, _input, show_type)


@command(
    context_settings=dict(ignore_unknown_options=True, allow_extra_args=True,)
)
@option("--lake_type", help="type of data lake ej: AWS, azure, GCS", default="AWS")
def list_buckets(lake_type: str):
    selected_buckets: list = []
    data_lake = AWSLake(lake_type=lake_type)
    data_lake.set_profile_name(profile_name="demo")

    buckets_list = data_lake.get_bucket_list()
    if len(buckets_list) > 0:
        secho("Buckets Found:", bg=BC, fg=SUCCESS_C)
        for index, lake in enumerate(buckets_list):
            secho(f"{index}. {lake}", bg=BC, fg=SUCCESS_C)
        secho("Do you want to select which buckets to use?", bg=BC, fg=FC)
        _input = confirm("")
        if _input is True:
            selected_buckets = get_select_buckets(buckets_list)
    else:
        secho("No buckets found", bg=BC, fg=WARNING_C)
        return

    if len(selected_buckets) > 0:
        secho("Do you want to perform an action with this buckets?", bg=BC, fg=FC)
        _input = confirm("")
        if _input is True:
            show_action_menu(selected_buckets, data_lake)
    else:
        secho("No buckets selected", bg=BC, fg=WARNING_C)
        return


def get_full_characteristics():
    pass


def get_percentages():
    pass


def get_pretty_map():
    pass


@group()
def cli():
    pass


if __name__ == "__main__":

    cli.add_command(list_buckets)
    cli()
