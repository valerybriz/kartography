from typing import Optional

import datetime
from kartography.core.settings import logger
from kartography.core.aws_lake import AWSLake
from kartography.core.errors import BucketNameError, ListIndexError
from kartography.core.utils import get_file_type, get_file_parent, prettify_json


def get_map(data_lake: AWSLake, bucket_name: str) -> dict:
    """Get's an intuitive map with the characteristics of an entire bucket"""
    return {"bucket": bucket_name}


def select_buckets(buckets: list, selected_list: str, command: str) -> (list, str):
    """select all of the buckets of double check if the selected ones exists"""
    logger.error(f"{command} {selected_list}")
    if "all" == command:
        return buckets, ""
    elif "only" == command:
        if selected_list == "":
            return [], "[Error] The selected values for buckets are not valid"
        else:
            try:
                return [buckets[int(index)] for index in selected_list.split(",")], ""
            except IndexError:
                return [], "[Error] The number selected doesn't match any bucket"
    elif "all-except" == command:
        if selected_list == "":
            return [], "[Error] The selected values for buckets are not valid"
        new_list = [bucket for index, bucket in enumerate(buckets) if str(index) not in selected_list.split(",")]
        return new_list, ""
    elif "no-dev" == command:
        return [], "Not implemented"
    elif "" == command or " " == command:
        return buckets, ""
    else:
        return [], "[Error] The command is not valid"


def get_count_distribution(bucket) -> dict:
    pass


def show_characteristics(current_lake: AWSLake, buckets: list, show_type: str):
    buckets_info = []
    for current_bucket in buckets:
        folder_results = {}
        folder_list = current_lake.get_folder_list(
            current_bucket, ignore_dev=False
        )
        for folder in folder_list:
            folder_char = get_folder_characteristics(
                    data_lake=current_lake, folder=folder, bucket_name=current_bucket
            )
            folder_results.update({folder: folder_char})

        buckets_info.append({current_bucket: folder_results})
    if show_type == "print":
        prettify_json(buckets_info)


def get_folder_characteristics(
    data_lake: AWSLake, folder: Optional[str], bucket_name: str
) -> dict:
    sub_folders = {}
    last_default_updated_date = datetime.datetime.strptime("01/01/1900", "%d/%m/%Y")
    current_paginator = data_lake.current_client.get_paginator("list_objects")
    for page in current_paginator.paginate(Bucket=bucket_name, Prefix=folder):
        for current_object in page["Contents"]:
            file_key = current_object.get("Key")

            if file_key[-1] != "/":
                # Is a file

                file_type = get_file_type(file_key)
                file_parent = get_file_parent(file_key)
                # Check if the parent folder of the file exists in the list
                current_sub_folder = sub_folders.get(file_parent)
                if current_sub_folder is None:
                    current_sub_folder = {
                        "file_count": 0,
                        "file_types": {},
                        "total_size": 0,
                        "last_updated": last_default_updated_date,
                    }
                current_sub_folder.update(
                    {"file_count": current_sub_folder.get("file_count") + 1}
                )
                # Check if the type exists in the count
                current_types_count = current_sub_folder.get("file_types").get(
                    file_type
                )
                if current_types_count is None or current_types_count < 1:
                    current_types_count = 0
                current_sub_folder.get("file_types").update(
                    {file_type: current_types_count + 1}
                )
                # total_size is in KB
                total_size = (
                    current_sub_folder.get("total_size")
                    + current_object.get("Size") / 1024.0
                )
                current_sub_folder.update({"total_size": total_size})

                current_file_modified_date = current_object.get("LastModified")
                last_updated_date = current_sub_folder.get("last_updated")
                if current_file_modified_date.date() > last_updated_date.date():
                    current_sub_folder.update(
                        {"last_updated": current_file_modified_date}
                    )

                sub_folders.update({file_parent: current_sub_folder})
            else:
                # Is a Folder
                pass

    return sub_folders


def is_object_sequence_by_date(data_lake: AWSLake, folder: str, bucket_name: str):
    """Check if the files inside this folder are stored 1 per date
    For example: 20201231.csv, 20210101.csv"""
    found_dates = []

    # found_dates.append(get_object_name_date(object_name))
    # for dates in found_dates:
    #   if date 1 day from next
    #  or date 1 month from next
    # true -> append to sequence list
    pass
