from typing import Optional
from kartography.core.settings import logger


class DataLake:
    bucket_name = None
    base_folder_name = None
    lake_type = ""  # AWS, AZURE, GCS

    def __init__(
        self,
        lake_type: str = "AWS",
        bucket_name: Optional[str] = "No Bucket Name",
        base_folder_name: Optional[str] = None,
    ) -> None:
        self.bucket_name = bucket_name
        self.base_folder_name = base_folder_name
        self.lake_type = lake_type
        logger.info(f"initializing lake with: {self.lake_type}")

    def credentials_ok(self) -> None:
        raise NotImplementedError

    def bucket_params_ok(self) -> None:
        raise NotImplementedError

    def get_bucket_list(self) -> None:
        raise NotImplementedError

    def get_folder_list(
        self, bucket_name: str, ignore_dev: bool = True, dev_word: str = "dev"
    ) -> None:
        raise NotImplementedError
