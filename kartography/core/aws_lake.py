import boto3
from kartography.core.data_lake import DataLake
from kartography.core.errors import CredentialsError, BucketNameError
from kartography.core.settings import logger


class AWSLake(DataLake):
    profile_name = ""
    access_id = ""
    access_secret = ""
    region_name = ""
    current_client = None

    def set_profile_name(self, profile_name: str):
        self.profile_name = profile_name

    def set_credentials(self, access_id: str, access_secret: str, region_name: str):
        self.access_id = access_id
        self.access_secret = access_secret
        self.region_name = region_name

    def get_session(self) -> boto3.Session:
        if self.profile_name != "":
            session = boto3.Session(profile_name=self.profile_name)
        elif self.access_id != "" != self.access_secret:
            session = boto3.Session(
                aws_access_key_id=self.access_id,
                aws_secret_access_key=self.access_secret,
                region_name=self.region_name,
            )
        else:
            raise CredentialsError()
        return session

    def get_s3_client(self) -> boto3.client:
        session = self.get_session()
        self.current_client = session.client("s3")
        return self.current_client

    def credentials_ok(self) -> bool:
        s3_client = self.get_s3_client()
        result = s3_client.list_buckets().get("Buckets")
        if len(result) > 0:
            return True
        else:
            return False

    def bucket_params_ok(self) -> bool:
        if self.bucket_name == "":
            raise BucketNameError()
        session = self.get_session()
        s3_client = session.client("s3")
        for bucket in s3_client.list_buckets().get("Buckets"):
            if self.bucket_name == bucket.get("Name"):
                return True
        return False

    def _get_bucket_list(self) -> list:
        """Obtain the list of buckets accessible from
        the user/role of the setup credentials"""
        if self.current_client is None:
            self.get_s3_client()
        return self.current_client.list_buckets().get("Buckets")

    def get_bucket_list(self) -> list:
        """Obtain the list of buckets accessible from
        the user/role of the setup credentials
        Response as str List"""
        bucket_list = self._get_bucket_list()
        return [bucket.get("Name") for bucket in bucket_list]

    def get_folder_list(
        self, bucket_name: str, ignore_dev: bool = True, dev_word: str = "dev"
    ) -> list:
        if self.current_client is None:
            self.get_s3_client()
        objects_list = self.current_client.list_objects_v2(
            Bucket=bucket_name, Delimiter="/"
        )
        folders_in_bucket = objects_list.get("CommonPrefixes")
        # TODO what happens when the amount of folders is too big? (what is too big)?
        if ignore_dev:
            non_dev_folders = []
            if folders_in_bucket is not None:
                for prefix in folders_in_bucket:
                    folder_key = prefix["Prefix"][:-1]
                    if not (
                        folder_key.startswith(dev_word) or folder_key.endswith(dev_word)
                    ):
                        non_dev_folders.append(folder_key)
            return non_dev_folders
        else:
            all_folders = []
            if folders_in_bucket is not None:
                logger.info(f"{len(folders_in_bucket)} Folders found in the current bucket")
                all_folders = [prefix["Prefix"][:-1] for prefix in folders_in_bucket]
            return all_folders
