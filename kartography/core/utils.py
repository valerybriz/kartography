import datetime
import re
import dateparser
from kartography.core.settings import logger

FILE_TYPES = {"csv_comp": "csv.gz", "csv": "csv"}
space = " "


def get_file_type(file_name: str) -> str:
    # TODO check if it works well with .csv.gz
    final_key = file_name.split("/")[-1]
    return ".".join(final_key.split(".")[1:])


def get_file_parent(key: str) -> str:
    parent = ""
    key_sliced = key.split("/")
    if len(key_sliced) > 1:
        parent = "/".join(key_sliced[:-1])

    return parent


def get_object_name_date(object_name: str) -> datetime.datetime:
    """Get the date contained in the name of the folder (if exists)
    For example: production_20201231/ -> 2020-12-31"""
    # TODO make it compatible with not complete dates
    pattern = "[0-9]{1,4}[\_|\-|\/|\|][0-9]{1,2}[\_|\-|\/|\|][0-9]{1,4}"
    no_dash_pattern = "[0-9]{4}[0-9]{1,2}[0-9]{1,2}|[0-9]{1,2}[0-9]{1,2}[0-9]{4}"
    aws_pattern = "[\w]{3,5}[\=]"
    found_date = None
    if len(re.findall(aws_pattern, object_name)) > 0:
        # check for dates partitioned by "year= month= day="
        object_name = [re.sub(aws_pattern, "", object_name)]

    matched_dates = re.findall(pattern, object_name)
    if len(matched_dates) > 0:
        found_date = dateparser.parse(matched_dates[0])
    else:
        matched_dates = re.findall(no_dash_pattern, object_name)
        if len(matched_dates) > 0:
            try:
                try:
                    new_date = datetime.datetime.strptime(matched_dates[0], "%Y%m%d")
                except ValueError:
                    new_date = datetime.datetime.strptime(matched_dates[0], "%d%m%Y")
                found_date = new_date
            except ValueError:
                logger.error("[get_folder_date_slice] Not a valid date to parse")

    if found_date is None:
        found_date = [
            dateparser.parse(cur_date) for cur_date in re.findall(pattern, object_name)
        ]
    return found_date


def convert_json_to_csv():
    pass


def store_file():
    # Should store the file in the folder you are right now
    pass


def prettify_single_json(single_json: dict, spaces_double) -> str:
    middle = ""
    start = "{ \n"
    end = f"{space * spaces_double}" + "}"
    spaces_double = spaces_double * 2
    for key, value in single_json.items():
        if isinstance(value, dict):
            value = prettify_single_json(value, spaces_double)
        middle = middle + f"{space * spaces_double}{key}: {value}, \n"
    return start + middle + end


def prettify_json(data: list) -> None:
    for data_object in data:
        bucket_name = list(data_object.keys())[0]
        print(f"Bucket Name: {bucket_name}")
        folders_data = data_object.get(bucket_name)
        if isinstance(folders_data, dict):
            json_pretty = prettify_single_json(folders_data, spaces_double=1)
            print(json_pretty)



