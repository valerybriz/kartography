import logging
from os import getenv

LOGGER_LEVEL = getenv("LOGGER_LEVEL")
logger = logging.getLogger()
logger.setLevel(LOGGER_LEVEL)
logging.basicConfig(format="%(asctime)s %(message)s")
