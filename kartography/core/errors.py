class CredentialsError(ValueError):
    """This error will raise when credentials are wrong set or invalid"""

    def __init__(self, message="The credentials are wrong or empty"):
        self.message = message
        super().__init__(self.message)


class BucketNameError(ValueError):
    """This error will raise when the name of the bucket is wrong or not found"""

    def __init__(self, message="Wrong Bucket Name or not found"):
        self.message = message
        super().__init__(self.message)


class ListIndexError(ValueError):
    """This error will raise when the index of a object list is wrong or not found"""

    def __init__(self, message="Wrong index number or not found"):
        self.message = message
        super().__init__(self.message)
