import boto3
from kartography.core.aws_lake import AWSLake
from kartography.commands.aws_map import (
    get_folder_characteristics,
    get_object_name_date,
)


default_aws_bucket = AWSLake(bucket_name="clearproducts", lake_type="AWS")


if __name__ == "__main__":
    if default_aws_bucket.current_client is None:
        default_aws_bucket.get_s3_client()
    # contenido = get_folder_characteristics(
    #    default_aws_bucket, "dim_products", "clearproducts"
    # )

    contenido = get_object_name_date("2020-01-01")
    print(contenido)
