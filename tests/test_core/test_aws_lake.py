import pytest
import datetime
from dateutil.tz import tzutc
from copy import copy
from os import getenv
from botocore.exceptions import ProfileNotFound

from kartography.core.aws_lake import AWSLake
from kartography.core.errors import CredentialsError, BucketNameError

default_aws_lake = AWSLake(lake_type="AWS", bucket_name="clearproducts")
wrong_aws_lake = AWSLake(lake_type="AWS", bucket_name="")
default_profile = "demo"
wrong_profile = "wrong"
default_credentials = {
    "access_id": getenv("AWS_ACCESS_KEY_ID"),
    "access_secret": getenv("AWS_SECRET_ACCESS_KEY"),
    "region": getenv("AWS_REGION"),
}


@pytest.mark.parametrize("temp_lake", [copy(default_aws_lake)])
def test_credentials_ok_profile(temp_lake):
    temp_lake.set_profile_name(default_profile)
    assert temp_lake.credentials_ok() is True


@pytest.mark.parametrize("temp_lake", [copy(default_aws_lake)])
def test_credentials_ok_keys(temp_lake):
    temp_lake.set_credentials(
        default_credentials.get("access_id"),
        default_credentials.get("access_secret"),
        default_credentials.get("region"),
    )
    assert temp_lake.credentials_ok() is True


@pytest.mark.parametrize("temp_lake", [copy(default_aws_lake)])
def test_credentials_ok_keys_wrong(temp_lake):
    temp_lake.set_credentials("", "", "")
    with pytest.raises(CredentialsError):
        temp_lake.credentials_ok()


@pytest.mark.parametrize("temp_lake", [wrong_aws_lake])
def test_wrong_credentials_ok(temp_lake):
    temp_lake.set_profile_name(wrong_profile)
    with pytest.raises(ProfileNotFound):
        temp_lake.credentials_ok()


@pytest.mark.parametrize("temp_lake", [wrong_aws_lake])
def test_wrong_specific_params(temp_lake):
    temp_lake.set_profile_name(default_profile)
    with pytest.raises(BucketNameError):
        temp_lake.bucket_params_ok()


@pytest.mark.parametrize("temp_lake", [copy(default_aws_lake)])
def test__get_bucket_list(temp_lake):
    temp_lake.set_profile_name(default_profile)
    buckets = temp_lake._get_bucket_list()
    assert {
        "CreationDate": datetime.datetime(2021, 11, 16, 20, 53, 5, tzinfo=tzutc()),
        "Name": "clearproducts",
    } in buckets
