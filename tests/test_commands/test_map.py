import pytest


from kartography.core.aws_lake import AWSLake
from kartography.commands.aws_map import get_map, select_buckets
from kartography.core.errors import BucketNameError, ListIndexError

default_aws_lake = AWSLake(bucket_name="my_AWS_bucket", lake_type="AWS")
default_aws_lake.set_profile_name("demo")
wrong_aws_lake = AWSLake(bucket_name="", lake_type="AWS")


@pytest.mark.parametrize(
    "bucket_list,selected_buckets,command",
    [
        (["clearproducts", "testlaken"], "0,1", "only"),
        (["clearproducts", "testlaken"], "", "all")
    ],
)
def test_select_buckets(bucket_list, selected_buckets, command):
    selected_list, error = select_buckets(bucket_list, selected_buckets, command)
    assert error == ""
    assert selected_list == ["clearproducts", "testlaken"]


@pytest.mark.parametrize(
    "bucket_list,selected_buckets,command",
    [
        (["clearproducts", "testlaken"], "", "only"),
        (["clearproducts", "testlaken"], "0,1", "na")
    ],
)
def test_select_buckets_wrong(bucket_list, selected_buckets, command):
    selected_list, error = select_buckets(bucket_list, selected_buckets, command)
    assert selected_list == []
    assert error == "[Error] The selected values for buckets are not valid" \
           or error == "[Error] The command is not valid"

